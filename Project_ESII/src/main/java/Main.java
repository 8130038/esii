//https://moodle.estg.ipp.pt/pluginfile.php/90276/mod_resource/content/3/jenkinsAPP.pdf

public class Main {
    int num1, num2;

    public Main(int num1, int num2) {
        this.num1 = num1;
        this.num2 = num2;

    }

    public int getNum1() {
        return num1;
    }

    public void setNum1(int num1) {
        this.num1 = num1;
    }

    public int getNum2() {
        return num2;
    }

    public void setNum2(int num2) {
        this.num2 = num2;
    }

    public int mult(int numx, int numy){
        int result = numx +  numy;
        return result;
    }
}
